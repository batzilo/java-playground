import java.util.Scanner;

public class TicTacToe {

    // Where every game is saved
    private static char matrix[][];

    // Current user symbol ( X is default )
    private static char symbol;

    // User and Computer Scores
    private static int score_u;
    private static int score_c;

    // Initialize matrix to contain only spaces --> ' '
    private static void init_matrix() {
        for (int i=0; i<3; i++) {
            for (int j=0; j<3; j++) {
                matrix[i][j] = ' ';
            }
        }
    }

    // Print the game state using delimiters
    private static void print_matrix() {
        System.out.printf("Game State is :\n");
        for (int i=0; i<3; i++) {
            for (int j=0; j<3; j++) {
                System.out.printf("%c ", matrix[i][j]);
                if (j<2) System.out.printf("| ");
            }
            System.out.printf("\n");
            if (i<2) System.out.println("----------");
        }
        System.out.printf("\n");
    }

    // Print the main menu to the user
    private static void print_menu() {
        System.out.printf("Main menu\n");
        System.out.printf("-----------\n");
        System.out.printf("1) Play Tic Tac Toe\n");
        System.out.printf("2) How to Play\n");
        System.out.printf("3) Change Icon\n");
        System.out.printf("4) Score\n");
        System.out.printf("5) Exit\n");
        System.out.printf("Your selection : ");
    }

    // Get input from the user. Return the selection (1-5)
    private static int get_menu_selection() {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        while ( a <= 0 || a >=6 ) {
            // The user gave invalid input, so prompt again
            System.out.printf("Wrong input! Try again :");
            a = input.nextInt();
        }
        // If input is ok, return it
        return a;
    }

    // Computer's turn. Randomly select row and column
    private static void c_play() {
        int a;
        int b;
        a = (int)(Math.random() * 3);
        b = (int)(Math.random() * 3);
        while ( matrix[a][b] != ' ' ) {
            // The randomly selected matix cell is not valid. Try again
            a = (int)(Math.random() * 3);
            b = (int)(Math.random() * 3);
        }
        // If valid, print a message...
        System.out.printf("Comp chose %d,%d\n", a+1, b+1);
        // ... and register computer's move
        if ( symbol == 'X' ) matrix[a][b] = 'O';
        else matrix[a][b] = 'X';
        return;
    }

    // User's turn. Get input for row and column
    private static void u_play() {
        Scanner input = new Scanner(System.in);
        System.out.printf("Your move: ");
        int a = input.nextInt();
        int b = input.nextInt();
        // make indices matrix-friendly (starting with 0)
        a--;
        b--;
        while ( a<0 || a>2 || b<0 || b>2 || matrix[a][b] != ' ' ) {
            // The user selected matrix cell is not valid. Prompt again
            System.out.printf("Wrong Input. Your move: ");
            a = input.nextInt();
            b = input.nextInt();
            // make indices matrix-friendly (starting with 0)
            a--;
            b--;
        }
        // If valid input, print a message and register user's move
        System.out.printf("User chose %d,%d\n", a+1, b+1);
        matrix[a][b] = symbol;
        return;
    }

    // Who plays now and who next?
    private static char next_move(char n) {
        if ( n == 'c' ) {
            // if called with 'c', it's computer's turn
            c_play();
            // so next is user
            return 'u';
        }
        else {
            // if called with 'u', it's user turn
            u_play();
            // so next is computer
            return 'c';
        }
    }

    // Check if we have a winner
    private static boolean winner() {
        boolean win;
        // check the horizontal axes
        // for every row
        for (int i=0; i<3; i++) {
            // let's say there is winner
            win = true;
            // mark the first row element for reference
            char first = matrix[i][0];
            // if it's empty, then no-winner
            if ( first == ' ' ) win = false;
            // for every other column in the current row find deal-breakers
            for (int j=1; j<3; j++) {
                // if found an element different from first, then no-win
                if (matrix[i][j] != first) win = false;
                // if found an empty element, then no-win
                if (matrix[i][j] == ' ') win = false;
            }
            // if current row did't have any deal-breakers, then win
            if (win) {
                // dummy message
                System.out.println("HORIZONTAL");
                // return true => there is a winner
                return true;
            }
        }
        // check vertical axes in a same manner
        for (int i=0; i<3; i++) {
            win = true;
            char first = matrix[0][i];
            if ( first == ' ' ) win = false;
            for (int j=1; j<3; j++) {
                if (matrix[j][i] != first) win = false;
                if (matrix[j][i] == ' ') win = false;
            }
            if (win) {
                System.out.println("VERTICAL");
                return true;
            }
        }
        // check the two diagonal axes
        if ( matrix[0][0] != ' ' && matrix[0][0] == matrix[1][1] && matrix[1][1] == matrix[2][2] ) return true;
        if ( matrix[0][2] != ' ' && matrix[0][2] == matrix[1][1] && matrix[1][1] == matrix[2][0] ) return true;

        // if no return true was executed, no win. so return false
        return false;
    }

    // Play TicTacToe method
    private static void play() {
        // reset game matrix
        init_matrix();
        // reset move counter
        int moves = 0;
        char n;
        // find who plays first
        if (symbol == 'X') n = 'u';
        else n = 'c';
        // while no winner and while there are moves available
        while ( !winner() && moves <= 9 ) {
            // print the matrix
            print_matrix();
            // next player makes move
            n = next_move(n);
            // a move was made
            moves++;
        }
        // print the final form
        print_matrix();
        // if still no winner, we have a TIE
        if ( !winner() ) {
            System.out.printf("TIE!\n");
        }
        // if winner, if next player is user, then computer has won
        else if ( n == 'u' ) {
            System.out.printf("Computer Wins!\n");
            // increase the score counter
            score_c++;
        }
        // if winner, if next player is computer, then user has won
        else {
            System.out.printf("User Wins!\n");
            // increase the score counter
            score_u++;
        }
        System.out.printf("\n\n\n");
        return;
    }

    // display the instructions on how to play
    private static void howto() {
        System.out.println("RTFM\n");
        System.out.printf("\n\n\n");
        return;
    }

    // change the user's symbol
    private static void change() {
        if ( symbol == 'X' ) symbol = 'O';
        else symbol = 'X';
        // print a message
        System.out.printf("Symbol changed to %c\n", symbol);
        System.out.printf("\n\n\n");
        return;
    }

    // print the scores
    private static void score() {
        System.out.printf("The score is:\n");
        System.out.printf("User :\t%d\n", score_u);
        System.out.printf("Comp :\t%d\n", score_c);
        System.out.printf("\n\n\n");
    }

    // Main menu function. Get input from user and call the appropriate method
    private static void main_menu() {
        int sel;
        while (true) {
            print_menu();
            // get_menu_selection returns a valid user selection
            sel = get_menu_selection();

            if (sel == 1) play();
            else if (sel == 2) howto();
            else if (sel == 3) change();
            else if (sel == 4) score();
            else return; // if sel == 5, we return to main and exit
        }
    }

    // Main method
    public static void main( String args[]) {

        // initialize matrix
        matrix = new char[3][3];

        // initialize scores
        score_u = 0;
        score_c = 0;

        // initialize symbol
        // by default, player has 'X'
        symbol = 'X';

        // go
        main_menu();

        // exit
        System.out.println("Bye!");
        return;
    }

}
