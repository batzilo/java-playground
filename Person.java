public class Person {

    // Person's first name
    private String name;

    // Person's last name
    private String surname;

    // The year when the Person was born
    private int yearOfBirth;

    public Person() {
        name = "<empty>";
        surname = "<empty>";
        yearOfBirth = 0;
    }

    public Person( String n, String s, int y ) {
        name = n;
        surname = s;
        yearOfBirth = y;
    }

    // get Person's fist name
    public String getName() {
        return name;
    }

    // set Person's first name
    public void setName( String n ) {
        name = n;
        return;
    }

    // get Person's last name
    public String getSurname() {
        return surname;
    }

    // set Person's last name
    public void setSurname( String s ) {
        surname = s;
        return;
    }

    //get Person's year of birth
    public int getYearOfBirth() {
        return yearOfBirth;
    }

    // set Person's year of birth
    public void setYearOfBirth( int y ) {
        yearOfBirth = y;
        return;
    }

    // calculate the Person's age
    public int calcAge( int yearNow ) {
        return yearNow - yearOfBirth;
    }
}
