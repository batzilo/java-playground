import java.util.Scanner;

public class AgeComparison {

    // print a message with the Person's info
    public static void printPersonInfo( Person p, int y ) {
        System.out.printf("Person '%s %s', born in %d is %d years old\n",
            p.getName(), p.getSurname(),
            p.getYearOfBirth(), p.calcAge(y) );
        return;
    }

    // compare two Person's ages and print a message
    public static void compareAges( Person a, Person b ) {
        if ( a.getYearOfBirth() == b.getYearOfBirth() )
            System.out.printf("%s and %s have the same age!\n",
                a.getName(), b.getName() );
        else
            System.out.printf("%s and %s don't have the same age!\n",
                a.getName(), b.getName() );
        return;
    }

    public static void main( String[] args ) {
        Scanner input = new Scanner( System.in );
        String name, surname;
        int year;

        // read first Person's info
        System.out.println("Give first person's data:");
        System.out.printf("Name : ");
        name = input.next();
        System.out.printf("Surname : ");
        surname = input.next();
        System.out.printf("Year Of Birth : ");
        year = input.nextInt();
        System.out.println();

        // Create a new Person, the easy way
        Person x = new Person( name, surname, year );

        // read second Person's info
        System.out.println("Give second person's data:");
        System.out.printf("Name : ");
        name = input.next();
        System.out.printf("Surname : ");
        surname = input.next();
        System.out.printf("Year Of Birth : ");
        year = input.nextInt();
        System.out.println();

        // Create a new Person, the hard way
        Person y = new Person();
        y.setName( name );
        y.setSurname( surname );
        y.setYearOfBirth( year );

        // Print info for every Person
        printPersonInfo( x, 2013 );
        printPersonInfo( y, 2013 );
        System.out.println();

        // compare the ages
        compareAges(x, y);
        System.out.println();
        return;
    }

}
