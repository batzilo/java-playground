import java.util.Scanner;

public class ManyHello {

    public static void main ( String[] args ) {
        Scanner input = new Scanner(System.in);

        System.out.println("How many times?");
        int N = input.nextInt();

        for (int i=0; i<N; i++) {
            System.out.println("Hello World!");
        }
        return;
    }

}
